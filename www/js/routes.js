angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.page2'
      2) Using $state.go programatically:
        $state.go('tabsController.page2');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/page2
      /page1/tab3/page2
  */
  .state('tabsController.page2', {
    url: '/page2',
	params: {
		contador: ""		
},
    views: {
      'tab1': {
        templateUrl: 'templates/page2.html',
        controller: 'page2Ctrl'
      },
      'tab3': {
        templateUrl: 'templates/page2.html',
        controller: 'page2Ctrl'
      }
    }
  })

  .state('page9', {
    url: '/page9',
    templateUrl: 'templates/page9.html',
    controller: 'page9Ctrl'
  })

  .state('tabsController.domicilio', {
    url: '/page3',
    views: {
      'tab2': {
        templateUrl: 'templates/domicilio.html',
        controller: 'domicilioCtrl'
      }
    }
  })

  .state('tabsController.page4', {
    url: '/page4',
    views: {
      'tab3': {
        templateUrl: 'templates/page4.html',
        controller: 'page4Ctrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.alcoholMetro'
      2) Using $state.go programatically:
        $state.go('tabsController.alcoholMetro');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab3/page8
      /page1/tab4/page8
  */
  .state('tabsController.alcoholMetro', {
    url: '/page8',
    views: {
      'tab3': {
        templateUrl: 'templates/alcoholMetro.html',
        controller: 'alcoholMetroCtrl'
      },
      'tab4': {
        templateUrl: 'templates/alcoholMetro.html',
        controller: 'alcoholMetroCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('login', {
    url: '/page5',
	params: {
		Usuario: "{{usuario2}}"		
},
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/page6',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('page7', {
    url: '/advertencia',
	params: {
		Usuario: "{{usuario2}}"		
},
    templateUrl: 'templates/page7.html',
    controller: 'page7Ctrl'
  })

$urlRouterProvider.otherwise('/page5')


});